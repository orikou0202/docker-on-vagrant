# ポートフォワーディング

ゲストマシンで立てたDockerコンテナにホストマシンから直接SSHしたい場合、ポートフォワーディングを使用します。  
通常はポートフォワーディングを使用する必要はありませんが、[Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)で使用することを想定して手順を記載します。  
また、コマンドに使用されるパスは環境によって異なる場合があるため、適宜読み替えてください。

## 1. configファイルの作成

```
vagrant ssh-config --host=vagrant >> C:\Users\<username>\.ssh\config
```

* `--host=vagrant`は任意。分かりやすくするため`vagrant`としています。

## 2. ゲストマシンに配置

```
scp .vagrant\machines\default\virtualbox\private_key vagrant:/home/vagrant/.ssh/
```

## 3. キーの権限変更

ゲストマシンにて権限の変更

```
chmod 600 ~/.ssh/private_key
```

## 4. Docker立ち上げ

Dockerを立ち上げる際、Dockerfileに以下の処理を追加してください。  
以下の設定ではDockerfileと同じ階層に `authorized_keys` (公開鍵) がある事を想定しています。  
コピーできればどの階層でも問題ありません。  
パッケージマネージャーは環境に合わせて読み替えてください。

```Dockerfile
RUN apt update
RUN apt-get install -y wget curl less vim openssh-server

COPY authorized_keys /tmp/authorized_keys

RUN mkdir /var/run/sshd ~/.ssh
RUN mv /tmp/authorized_keys ~/.ssh/authorized_keys && \
    chmod 600 ~/.ssh/authorized_keys

RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22

CMD /usr/sbin/sshd -D
```

## 5. 手動SSH (known_hosts作成)

コンテナが立ち上がったら、SSH接続します。

```bash
ssh root@127.0.0.1 -p 10000 -i ~/.ssh/private_key
```

## 6. configファイルの更新

**ホストマシン** の `.ssh/config` を開いて、以下を追加します。

```
Host docker
  Hostname 127.0.0.1
  User root
  Port 10000
  ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe -W %h:%p vagrant
  IdentityFile C:\Users\<username>\work\vagrant\centos-7-docker\.vagrant\machines\default\virtualbox\private_key
```

* `IdentityFile` はホストマシンにある鍵のパスを指定してください。<br>既にvagrantの設定が記載されているはずですので、そのままコピーしても問題ありません。
* `Host` は任意。分かりやすくするため `docker` としています。  
* `ProxyCommand` の末尾の `vagrant` はゲストマシンのホスト名です。  

## 7. Remote Development

リモートエクスプローラーに `vagrant` と `docker` が追加されて且つアクセスできれば正しく導入できています。
