# 共有フォルダのファイル操作検知

Visual Studio Codeを使用している場合は、[Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)を使用することも検討してみてください。  

デフォルト（当リポジトリ）の設定では、ゲストマシン側は `/vagrant-data` 配下のファイルの変更を検知することができません。（変更の適用はされます。）  
ゲストマシンに`inotify-tools`を導入し、監視したところ、検知しませんでした。  
変更を検知したい場合は `Vagrant Rsync Synced Folder` を使用します。

`Vagrantfile`
```
config.vm.synced_folder "./vagrant_data", "/vagrant_data", :mount_options => ['dmode=777', 'fmode=777']
```

上記の設定に以下の文字列を追加します。

```
type: "rsync", rsync_auto: true
```

マシンを再作成した後、以下のコマンドを実行します。

```bash
vagrant rsync-auto
```

こうすることで、共有フォルダのファイルを監視するようになります。  
`Ctrl + C`で中止することができますが、中止した時点で監視はストップされるため、常に監視が必要な場合は実行状態を保つ必要があります。