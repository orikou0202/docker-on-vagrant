# シンボリックリンクの作成許可

`管理者権限`で`Powershell`を起動し、以下のコマンドを実行します。

```bash
fsutil behavior set SymlinkEvaluation L2L:1 R2R:1 L2R:1 R2L:1
```

以下のコマンドで有効になっていることを確認します。

```bash
fsutil behavior query symlinkevaluation
ローカルからローカルへのシンボリック リンクは有効です。
ローカルからリモートへのシンボリック リンクは有効です。
リモートからローカルへのシンボリック リンクは有効です。
リモートからリモートへのシンボリック リンクは有効です。
```