
Vagrant.configure("2") do |config|

  # config.vbguest.auto_update = false

  config.vm.box = "generic/centos8"
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "forwarded_port", guest: 10000, host: 10000
  config.vm.synced_folder "./vagrant-data", "/vagrant-data", :mount_options => ['dmode=777', 'fmode=777']
  # config.vm.synced_folder "./vagrant-data", "/vagrant-data", :mount_options => ['dmode=777', 'fmode=777'], type: "rsync", rsync_auto: true

  config.disksize.size = "50GB"

  config.vm.provider "virtualbox" do |vb|
    
    vb.name = "vagrant_on_docker"
    vb.cpus = "8"
    vb.memory = "8192"
    vb.customize [
      "modifyvm", :id,
      "--hwvirtex", "on",
      "--nestedpaging", "on",
      "--largepages", "on",
      "--ioapic", "on",
      "--pae", "on",
      "--paravirtprovider", "kvm",
      "--cableconnected1", "on",
      "--nictype1",  "virtio",
      "--nictype2",  "virtio",
      "--natdnshostresolver1", "on",
      "--natdnsproxy1", "on"
    ]

    # symbolic link
    vb.customize [
      "setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant-data", "1"
    ]
    
  end

  config.vm.provision "shell", inline: <<-SHELL

    # package 
    dnf -y update

    # docker
    sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo dnf config-manager --enable docker-ce-nightly
    sudo dnf config-manager --enable docker-ce-test
    sudo dnf config-manager --disable docker-ce-nightly
    sudo dnf install -y docker-ce docker-ce-cli containerd.io
    sudo systemctl start docker

    # docker-compose
    sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

    # git
    # sudo dnf -y install git

    # node & yarn
    # sudo curl -sL https://rpm.nodesource.com/setup_12.x | bash -
    # sudo dnf install -y nodejs
    # sudo curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
    # sudo dnf install -y yarn

    # vue@cli
    # sudo yarn global add @vue/cli
    # sudo yarn global upgrade --latest @vue/cli

  SHELL


  config.vm.provision "shell", run: "always", inline: <<-SHELL

    # docker startup
    sudo systemctl start docker

  SHELL

end
