## 要件

- [Vagrant](https://www.vagrantup.com/) がホストコンピュータにインストールされていること
    - vagrantに以下のプラグインが導入されていること
        - `vagrant-disksize`
        - `vagrant-vbguest`
- [VirtualBox](https://www.virtualbox.org/) がホストコンピュータにインストールされていること
- `Hyper-V`が無効になっていること
- ホストマシンで[シンボリックリンク作成の許可](#シンボリックリンクの作成許可)がされていること

Windowsで使用することを想定しています。

## 使い方

共有フォルダ `vagrant-data` 配下にdockerで動かしたいプロジェクトを配置するだけです。  

### 起動

```bash
vagrant up --provision
```

### 再起動

```bash
vagrant reload
```

### 削除

```bash
vagrant destroy
```

## ゲストマシン詳細

詳しくは [Vagrantfile](Vagrantfile) を参照。  
コア数やメモリ容量などはホストマシンによって適宜変更してください。

|項目|内容|
|-|-|
|OS|CentOS 8|
|IPアドレス|192.168.33.10|
|共有フォルダ|/vagrant-data|
|CPU数|8|
|メモリ容量|8GB (8192MB)|
|ディスク容量|50GB|

導入済みソフトウェア
- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- ~~[git](https://git-scm.com/)~~
- ~~[node](https://nodejs.org/)~~
    - ~~[yarn](https://classic.yarnpkg.com/)~~
    - ~~[vue@cli](https://cli.vuejs.org/)~~

## フォルダ構成

```
.
├── vagrant-data
│   └── hello-world
├── tips
├── README.md
├── Vagrantfile
└── Vagrantfile.bak
```

|フォルダ・ファイル名|説明|
|-|-|
|vagrant-data|Dockerのプロジェクトを配置するフォルダ|
|tips|Tips|
|hello-world|サンプルプロジェクト|
|README.md|要件・セットアップ等の説明ファイル|
|Vagrantfile|vagrantの設定ファイル|
|Vagrantfile.bak|vagrantの初期設定ファイル|


## Tips

- [共有フォルダのファイル操作検知](tips/file-monitoring.md)
- [ポートフォワーディング](tips/port-forward.md)
- [シンボリックリンク](tips/symbolic-link.md)
